﻿using AutoMapper;
using Lab2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public class Mapping
    {
        public void Create()
        {
            Mapper.CreateMap<DataModel, DataViewModel>();
            Mapper.CreateMap<DataViewModel, DataModel>();

            Mapper.CreateMap<Car, CarViewModel>();
            Mapper.CreateMap<CarViewModel, Car>();

            Mapper.CreateMap<Reservation, ReservationViewModel>();
            Mapper.CreateMap<ReservationViewModel, Reservation>();
        }
    }
}
