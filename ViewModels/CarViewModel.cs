﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public class CarViewModel : ViewModelBase
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged("Id");
            }
        }

        private string _model;
        public string Model
        {
            get { return _model; }
            set
            {
                _model = value;
                OnPropertyChanged("Model");
            }
        }

        private string _number;
        public string Number
        {
            get { return _number; }
            set
            {
                _number = value;
                OnPropertyChanged("Number");
            }
        }

        private int _price;
        public int Price
        {
            get { return _price; }
            set
            {
                _price = value;
                OnPropertyChanged("Price");
            }
        }

        private bool _isAvaliable;
        public bool IsAvaliable
        {
            get { return _isAvaliable; }
            set
            {
                _isAvaliable = value;
                OnPropertyChanged("IsAvaliable");
            }
        }

        private int _tankVolume;
        public int TankVolume
        {
            get { return _tankVolume; }
            set
            {
                _tankVolume = value;
                OnPropertyChanged("TankVolume");
            }
        }

        private string _fuel;
        public string Fuel
        {
            get { return _fuel; }
            set
            {
                _fuel = value;
                OnPropertyChanged("Fuel");
            }
        }
    }
}
