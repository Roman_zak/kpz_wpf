﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public class ReservationViewModel : ViewModelBase
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set 
            { 
                _id = value;
                OnPropertyChanged("Id");
            }
        }

        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                OnPropertyChanged("UserName");
            }
        }

        private int _carId;
        public int CarId
        {
            get { return _carId; }
            set
            {
                _carId = value;
                OnPropertyChanged("CarId");
            }
        }

        private DateTime _takeDate;
        public DateTime TakeDate
        {
            get { return _takeDate; }
            set
            {
                _takeDate = value;
                OnPropertyChanged("TakeDate");
            }
        }

        private DateTime _returnDate;
        public DateTime ReturnDate
        {
            get { return _returnDate; }
            set
            {
                _returnDate = value;
                OnPropertyChanged("ReturnDate");
            }
        }

        private int _nOfDays;
        public int NOfDays
        {
            get { return _nOfDays; }
            set
            {
                _nOfDays = value;
                OnPropertyChanged("NOfDays");
            }
        }

        private int _sum;
        public int Sum
        {
            get { return _sum; }
            set
            {
                _sum = value;
                OnPropertyChanged("Sum");
            }
        }

        private Status _reservationStatus;
        public Status ReservationStatus
        {
            get { return _reservationStatus; }
            set
            {
                _reservationStatus = value;
                OnPropertyChanged("ReservationStatus");
            }
        }
    }
}
