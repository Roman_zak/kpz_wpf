﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Lab2
{
    public class DataViewModel : ViewModelBase
    {
        public DataViewModel()
        {
            SetControlVisibility = new Command(ControlVisibility);
            CompleteReservationCommand = new Command(CompleteReservation);
            CanceleReservationCommand = new Command(CanceleReservation);
            DeleteCarCommand = new Command(DeleteCar);
        }

        private string _visibleControl = "Reservations";

        public string VisibleControl
        {
            get { return _visibleControl; }
            set
            {
                _visibleControl = value;
                OnPropertyChanged("VisibleControl");
            }
        }

        public ICommand SetControlVisibility { get; set; }

        public void ControlVisibility(object args)
        {
            VisibleControl = args.ToString();
        }

        private ReservationViewModel _selectedReservation;
        public ReservationViewModel SelectedReservation
        {
            get { return _selectedReservation; }
            set
            {
                _selectedReservation = value;
                OnPropertyChanged("SelectedReservation");
            }
        }
        private CarViewModel _selectedCar;
        public CarViewModel SelectedCar
        {
            get { return _selectedCar; }
            set
            {
                _selectedCar = value;
                OnPropertyChanged("SelectedCar");
            }
        }

        public ICommand CompleteReservationCommand { get; set; }
        public void CompleteReservation(object args)
        {
            if(SelectedReservation != null)
                SelectedReservation.ReservationStatus = Status.Completed;
        }
        public ICommand CanceleReservationCommand { get; set; }
        public void CanceleReservation(object args)
        {
            if (SelectedReservation != null)
                SelectedReservation.ReservationStatus = Status.Canceled;
        }
        public ICommand DeleteCarCommand { get; set; }
        public void DeleteCar(object args)
        {
            if (SelectedCar != null)
                Cars.Remove(SelectedCar);
        }
        private ObservableCollection<CarViewModel> _сars;
        public ObservableCollection<CarViewModel> Cars
        {
            get { return _сars; }
            set 
            {
                _сars = value;
                OnPropertyChanged("Cars");
             }
        }

        private ObservableCollection<ReservationViewModel> _reservations;
        public ObservableCollection<ReservationViewModel> Reservations
        {
            get { return _reservations; }
            set
            {
                _reservations = value;
                OnPropertyChanged("Reservations");
            }
        }


    }
}
